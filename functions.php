<?php
/**
 * ibid functions and definitions
 *
 * @package ibid
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
    $content_width = 640; /* pixels */
}

if ( ! function_exists( 'ibid_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ibid_setup() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on ibid, use a find and replace
     * to change 'ibid' to the name of your theme in all the template files
     */
    load_theme_textdomain( 'ibid', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'woocommerce', array(
        'gallery_thumbnail_image_width' => 200,
        'woocommerce_thumbnail' => 768,
    ));
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );


    /**
    * Disable WPBakery frontend editor
    *
    * @since 1.2
    */
    if (class_exists('Vc_Manager')) {
        vc_disable_frontend();
    }
    
    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary Menu', 'ibid' ),
    ) );

    if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
        if (ibid_redux('ibid_header_category_menu') != '0') {
            register_nav_menus( array(
                'category' => esc_html__( 'Category Menu', 'ibid' ),
            ) );
        }
    }

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
    ) );

    /*
     * Enable support for Post Formats.
     */
    add_theme_support( 'post-formats', array(
        'aside', 'image', 'video', 'quote', 'link',
    ) );

    // Set up the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'ibid_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
    ) ) );
}
endif; // ibid_setup
add_action( 'after_setup_theme', 'ibid_setup' );

/**
 * Register widget area.
 *
 */
function ibid_widgets_init() {

    global $ibid_redux;

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'ibid' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Sidebar 1', 'ibid' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );
    if ( class_exists( 'WooCommerce' ) ) {
        register_sidebar( array(
            'name'          => esc_html__( 'WooCommerce sidebar', 'ibid' ),
            'id'            => 'woocommerce',
            'description'   => esc_html__( 'Used on WooCommerce pages', 'ibid' ),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h1 class="widget-title">',
            'after_title'   => '</h1>',
        ) );
    }

    if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
        if (isset($ibid_redux['dynamic_sidebars']) && !empty($ibid_redux['dynamic_sidebars'])){
            foreach ($ibid_redux['dynamic_sidebars'] as &$value) {
                $id           = str_replace(' ', '', $value);
                $id_lowercase = strtolower($id);
                if ($id_lowercase) {
                    register_sidebar( array(
                        'name'          => esc_html($value),
                        'id'            => esc_html($id_lowercase),
                        'description'   => esc_html($value),
                        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                        'after_widget'  => '</aside>',
                        'before_title'  => '<h1 class="widget-title">',
                        'after_title'   => '</h1>',
                    ) );
                }
            }
        }

        if (isset($ibid_redux['ibid_number_of_footer_columns'])) {
            for ($i=1; $i <= intval( $ibid_redux['ibid_number_of_footer_columns'] ) ; $i++) { 
                register_sidebar( array(
                    'name'          => esc_html__( 'Footer ', 'ibid' ).esc_html($i),
                    'id'            => 'footer_column_'.esc_html($i),
                    'description'   => esc_html__( 'Footer sidebar to show widgets by different column grid.', 'ibid' ),
                    'before_widget' => '<aside id="%1$s" class="widget vc_column_vc_container %2$s">',
                    'after_widget'  => '</aside>',
                    'before_title'  => '<h1 class="widget-title">',
                    'after_title'   => '</h1>',
                ) );
            }
        }
    }
}
add_action( 'widgets_init', 'ibid_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function ibid_scripts() {

    //STYLESHEETS
    wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/css/font-awesome.min.css' );
    wp_enqueue_style( 'ibid-responsive', get_template_directory_uri().'/css/responsive.css' );
    wp_enqueue_style( 'ibid-media-screens', get_template_directory_uri().'/css/media-screens.css' );
    wp_enqueue_style( 'owl-carousel', get_template_directory_uri().'/css/owl.carousel.css' );
    wp_enqueue_style( 'owl-theme', get_template_directory_uri().'/css/owl.theme.css' );
    wp_enqueue_style( 'animate', get_template_directory_uri().'/css/animate.css' );
    wp_enqueue_style( 'simple-line-icons', get_template_directory_uri().'/css/simple-line-icons.css' );
    wp_enqueue_style( 'ibid-styles', get_template_directory_uri().'/css/style.css' );
    wp_enqueue_style( 'ibid-skin-default', get_template_directory_uri().'/css/skin-colors/skin-default.css' );
    wp_enqueue_style( 'ibid-style', get_stylesheet_uri() );
    wp_enqueue_style( 'ibid-gutenberg-frontend', get_template_directory_uri().'/css/gutenberg-frontend.css' );

    //SCRIPTS
    wp_enqueue_script( 'modernizr-custom', get_template_directory_uri() . '/js/modernizr.custom.js', array('jquery'), '2.6.2', true );
    wp_enqueue_script( 'classie', get_template_directory_uri() . '/js/classie.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'jquery-form', get_template_directory_uri() . '/js/jquery.form.js', array('jquery'), '3.51', true );
    wp_enqueue_script( 'jquery-ketchup', get_template_directory_uri() . '/js/jquery.ketchup.all.min.js', array('jquery'), '0.3.1', true );
    wp_enqueue_script( 'jquery-validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array('jquery'), '1.13.1', true );
    wp_enqueue_script( 'jquery-sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'uisearch', get_template_directory_uri() . '/js/uisearch.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'jquery-flatshadow', get_template_directory_uri() . '/js/jquery.flatshadow.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'jquery-parallax', get_template_directory_uri() . '/js/jquery.parallax.js', array('jquery'), '1.1.3', true );
    wp_enqueue_script( 'jquery-appear', get_template_directory_uri() . '/js/count/jquery.appear.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'jquery-countTo', get_template_directory_uri() . '/js/count/jquery.countTo.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'modernizr-viewport', get_template_directory_uri() . '/js/modernizr.viewport.js', array('jquery'), '2.6.2', true );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.1', true );
    wp_enqueue_script( 'animate', get_template_directory_uri() . '/js/animate.js', array('jquery'), '1.0.0', true );
    // Color picker for dokan
    if (class_exists('Dokan_Template_Products')) {
        wp_enqueue_script( 'jquery-ui-datepicker' );
    }
    wp_enqueue_script( 'ibid-custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'ibid-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array('jquery'), '20130115', true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'ibid_scripts' );


/**
 * Enqueue scripts and styles for admin dashboard.
 */
if (!function_exists('ibid_enqueue_admin_scripts')) {
    function ibid_enqueue_admin_scripts( $hook ) {
        wp_enqueue_style( 'admin-style-css', get_template_directory_uri().'/css/admin-style.css' );
        if ( 'post.php' == $hook || 'post-new.php' == $hook ) {
            wp_enqueue_style( 'ibid-admin-style', get_template_directory_uri().'/css/admin-style.css' );
        }
    }
    add_action('admin_enqueue_scripts', 'ibid_enqueue_admin_scripts');
}


/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/**
 * Include the TGM_Plugin_Activation class.
 */
require get_template_directory().'/inc/tgm/include_plugins.php';
/**
 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
 */
add_action( 'vc_before_init', 'ibid_vcSetAsTheme' );
function ibid_vcSetAsTheme() {
    vc_set_as_theme( true );
}


add_action( 'vc_base_register_front_css', 'ibid_enqueue_front_css_foreever' );

function ibid_enqueue_front_css_foreever() {
    wp_enqueue_style( 'js_composer_front' );
}

/* ========= LOAD - REDUX - FRAMEWORK ===================================== */
require_once(get_template_directory() . '/redux-framework/ibid-config.php');

// CUSTOM FUNCTIONS
require_once(get_template_directory() . '/inc/custom-functions.php');
require_once(get_template_directory() . '/inc/custom-functions.header.php');
require_once get_template_directory() . '/inc/custom-functions.gutenberg.php';
if (class_exists('Dokan_Template_Products')) {
    require_once get_template_directory() . '/inc/custom-functions.dokan.php';
}

/* ========= CUSTOM COMMENTS ===================================== */
require get_template_directory() . '/inc/custom-comments.php';

/* ========= RESIZE IMAGES ===================================== */
add_image_size( 'ibid_member_pic350x350',        350, 350, true );
add_image_size( 'ibid_testimonials_pic110x110',  110, 110, true );
add_image_size( 'ibid_portfolio_pic400x400',     400, 400, true );
add_image_size( 'ibid_featured_post_pic500x230', 500, 230, true );
add_image_size( 'ibid_related_post_pic500x300',  500, 300, true );
add_image_size( 'ibid_post_pic700x450',          700, 450, true );
add_image_size( 'ibid_portfolio_pic500x350',     500, 350, true );
add_image_size( 'ibid_portfolio_pic700x450',     700, 450, true );
add_image_size( 'ibid_single_post_pic1200x500',   1200, 500, true );
add_image_size( 'ibid_posts_1100x600',     1100, 600, true );
add_image_size( 'ibid_post_widget_pic70x70',     70, 70, true );
add_image_size( 'ibid_pic100x75',                100, 75, true );


/* ========= LIMIT POST CONTENT ===================================== */
function ibid_excerpt_limit($string, $word_limit) {
    $words = explode(' ', $string, ($word_limit + 1));
    if(count($words) > $word_limit) {
        array_pop($words);
    }
    return implode(' ', $words);
}

/* ========= BREADCRUMBS ===================================== */
if (!function_exists('ibid_breadcrumb')) {
    function ibid_breadcrumb() {
        global $ibid_redux;

         if (  class_exists( 'ReduxFrameworkPlugin' ) ) {
            if ( !$ibid_redux['ibid-enable-breadcrumbs'] ) {
               return false;
            }
        }

        $delimiter = '';
        //text for the 'Home' link
        $name = esc_html__("Home", "ibid");
            if (!is_home() && !is_front_page() || is_paged()) {
                global $post;
                global $product;
                $home = home_url();
                echo '<li><a href="' . esc_url($home) . '">' . esc_html($name) . '</a></li> ' . esc_html($delimiter) . '';
            if (is_category()) {
                global $wp_query;
                $cat_obj = $wp_query->get_queried_object();
                $thisCat = $cat_obj->term_id;
                $thisCat = get_category($thisCat);
                $parentCat = get_category($thisCat->parent);
                    if ($thisCat->parent != 0)
                echo(get_category_parents($parentCat, true, '' . esc_html($delimiter) . ''));
                echo   '<li class="active">' . esc_html(single_cat_title('', false)) .  '</li>';
            } elseif (is_day()) {
                echo '<li><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a></li> ' . esc_html($delimiter) . '';
                echo '<li><a href="' . esc_url(get_month_link(get_the_time('Y'), get_the_time('m'))) . '">' . get_the_time('F') . '</a></li> ' . esc_html($delimiter) . ' ';
                echo  '<li class="active">' . get_the_time('d') . '</li>';
            } elseif (is_month()) {
                echo '<li><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . get_the_time('Y') . '</a></li> ' . esc_html($delimiter) . '';
                echo  '<li class="active">' . get_the_time('F') . '</li>';
            } elseif (is_year()) {
                echo  '<li class="active">' . get_the_time('Y') . '</li>';
            } elseif (is_attachment()) {
                echo  '<li class="active">';
                the_title();
                echo '</li>';
            } elseif (class_exists( 'WooCommerce' ) && is_shop()) {
                echo  '<li class="active">';
                echo esc_html__('Shop','ibid');
                echo '</li>';
            }elseif (class_exists('WooCommerce') && is_product()) {
                if (get_the_category()) {
                    $cat = get_the_category();
                    $cat = $cat[0];
                    echo '<li>' . get_category_parents($cat, true, ' ' . esc_html($delimiter) . '') . '</li>';
                }
                echo  '<li class="active">';
                the_title();
                echo  '</li>';
            } elseif (is_single()) {
                if (get_the_category()) {
                    $cat = get_the_category();
                    $cat = $cat[0];
                    echo '<li>' . get_category_parents($cat, true, ' ' . esc_html($delimiter) . '') . '</li>';
                }
                echo  '<li class="active">';
                the_title();
                echo  '</li>';
            } elseif (is_page() && !$post->post_parent) {
                echo  '<li class="active">';
                the_title();
                echo  '</li>';
            } elseif (is_page() && $post->post_parent) {
                $parent_id = $post->post_parent;
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_page($parent_id);
                    $breadcrumbs[] = '<li><a href="' . esc_url(get_permalink($page->ID)) . '">' . get_the_title($page->ID) . '</a></li>';
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                foreach ($breadcrumbs as $crumb)
                    echo  wp_kses($crumb, 'link') . ' ' . esc_html($delimiter) . ' ';
                echo  '<li class="active">';
                the_title();
                echo  '</li>';
            } elseif (is_search()) {
                echo  '<li class="active">' . get_search_query() . '</li>';
            } elseif (is_tag()) {
                echo  '<li class="active">' . single_tag_title( '', false ) . '</li>';
            } elseif (is_author()) {
                global $author;
                $userdata = get_userdata($author);
                echo  '<li class="active">' . esc_html($userdata->display_name) . '</li>';
            } elseif (is_404()) {
                echo  '<li class="active">' . esc_html__('404 Not Found','ibid') . '</li>';
            }
            if (get_query_var('paged')) {
                if (is_home() || is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
                    echo  '<li class="active">';
                echo esc_html__('Page','ibid') . ' ' . get_query_var('paged');
                if (is_home() || is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
                    echo  '</li>';
            }
        }
    }
}
// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
function ibid_woocommerce_header_add_to_cart_fragment( $fragments ) {
    ob_start();
    ?>
    <a class="cart-contents" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php esc_attr_e( 'View your shopping cart','ibid' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->cart_contents_count, 'ibid' ), WC()->cart->cart_contents_count ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a>
    <?php
    $fragments['a.cart-contents'] = ob_get_clean();
    return $fragments;
} 
add_filter( 'woocommerce_add_to_cart_fragments', 'ibid_woocommerce_header_add_to_cart_fragment' );

/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'ibid_rename_tabs', 98 );
function ibid_rename_tabs( $tabs ) {

    $tabs['more_seller_product']['title'] = esc_html__( 'More from Vendor', 'ibid' );    // Rename the additional information tab

    return $tabs;

}

add_filter( 'woocommerce_widget_cart_is_hidden', 'ibid_always_show_cart', 40, 0 );
function ibid_always_show_cart() {
    return false;
}




// SINGLE PRODUCT
// Unhook functions
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

// Hook functions
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 5 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

/* ========= PAGINATION ===================================== */
if ( ! function_exists( 'ibid_pagination' ) ) {
    function ibid_pagination($query = null) {

        if (!$query) {
            global $wp_query;
            $query = $wp_query;
        }
        
        $big = 999999999; // need an unlikely integer
        $current = (get_query_var('paged')) ? get_query_var('paged') : ((get_query_var('page')) ? get_query_var('page') : '1');
        echo paginate_links( 
            array(
                'base'          => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format'        => '?paged=%#%',
                'current'       => max( 1, $current ),
                'total'         => $query->max_num_pages,
                'prev_text'     => esc_html__('&#171;','ibid'),
                'next_text'     => esc_html__('&#187;','ibid'),
            ) 
        );
    }
}

/* ========= SEARCH FOR POSTS ONLY ===================================== */
function ibid_search_filter($query) {
    if ($query->is_search && !isset($_GET['post_type'])) {
        $query->set('post_type', 'post');
    }
    return $query;
}
if( !is_admin() ){
    add_filter('pre_get_posts','ibid_search_filter');
}

/* ========= CHECK FOR PINGBACKS ===================================== */
function ibid_post_has( $type, $post_id ) {
    $comments = get_comments('status=approve&type=' . esc_html($type) . '&post_id=' . esc_html($post_id) );
    $comments = separate_comments( $comments );
    return 0 < count( $comments[ $type ] );
}

/* ========= REGISTER FONT-AWESOME TO REDUX ===================================== */
if (!function_exists('ibid_register_fontawesome_to_redux')) {
    function ibid_register_fontawesome_to_redux() {
        wp_register_style( 'font-awesome', get_template_directory_uri().'/css/font-awesome.min.css', array(), time(), 'all' );  
        wp_enqueue_style( 'font-awesome' );
    }
    add_action( 'redux/page/redux_demo/enqueue', 'ibid_register_fontawesome_to_redux' );
}


/* Custom functions for woocommerce */
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash' );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail' );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

if (!function_exists('ibid_woocommerce_show_top_custom_block')) {
    function ibid_woocommerce_show_top_custom_block() {
        $args = array();
        global $product;
        global $ibid_redux;
        echo '<div class="thumbnail-and-details">';    
                  
            wc_get_template( 'loop/sale-flash.php' );
            
            echo '<div class="overlay-container">';
                echo '<div class="thumbnail-overlay"></div>';
                echo '<div class="overlay-components">';

                    echo '<div class="component add-to-cart">';
                        woocommerce_template_loop_add_to_cart();
                    echo '</div>';

                    if ( class_exists( 'YITH_WCWL' ) ) {
    	                echo '<div class="component wishlist">';
    	                    echo do_shortcode( "[yith_wcwl_add_to_wishlist]" );
    	                echo '</div>';
    	            }

                    if (  class_exists( 'YITH_WCQV' ) ) {
                        echo '<div class="component quick-view">';
                            echo '<a href="'.esc_url('#').'" class="button yith-wcqv-button" data-tooltip="'.esc_attr__('Quickview', 'ibid').'" data-product_id="' . esc_attr(yit_get_prop( $product, 'id', true )) . '"><i class="fa fa-search"></i></a>';
                        echo '</div>';
                    }

                echo '</div>';
            echo '</div>';

            echo '<a class="woo_catalog_media_images" title="'.the_title_attribute('echo=0').'" href="'.esc_url(get_the_permalink(get_the_ID())).'">'.woocommerce_get_product_thumbnail();
                if (class_exists('ReduxFrameworkPlugin')) {
	                if (ibid_redux('ibid-archive-secondary-image-on-hover') != '0' && ibid_redux('ibid-archive-secondary-image-on-hover') != '') {
	                	# code...
		                // SECONDARY IMAGE (FIRST IMAGE FROM WOOCOMMERCE PRODUCT GALLERY)
		                $product = new WC_Product( get_the_ID() );
		                $attachment_ids = $product->get_gallery_image_ids();

		                if ( is_array( $attachment_ids ) && !empty($attachment_ids) ) {
		                    $first_image_url = wp_get_attachment_image_url( $attachment_ids[0], 'ibid_portfolio_pic400x400' );
		                    echo '<img class="woo_secondary_media_image" src="'.esc_url($first_image_url).'" alt="'.the_title_attribute('echo=0').'" />';
		                }
	                }
                }
            echo '</a>';
        echo '</div>';
    }
    add_action( 'woocommerce_before_shop_loop_item_title', 'ibid_woocommerce_show_top_custom_block' );
}


if (!function_exists('ibid_woocommerce_show_price_and_review')) {
    function ibid_woocommerce_show_price_and_review() {
        $args = array();
        global $product;
        global $ibid_redux;

        echo '<div class="details-container">';
            echo '<div class="details-price-container details-item">';
                if (class_exists( 'WooCommerce_simple_auction' )) {
                    $_product = wc_get_product( get_the_ID() );
                    if( $_product->is_type( 'auction' ) ) {
                        echo esc_html__('Current Bid: ', 'ibid');
                    }
                }
                wc_get_template( 'loop/price.php' );
                
                echo '<div class="details-review-container details-item">';
                    wc_get_template( 'loop/rating.php' );
                echo '</div>';

                echo '<div class="bottom-components">';
                    // Add to cart button
                    echo '<div class="component add-to-cart">';
                        echo woocommerce_template_loop_add_to_cart();
                    echo '</div>';
                    // Wishlist button
                    if ( class_exists( 'YITH_WCWL' ) ) {
                        echo '<div class="component wishlist">';
                            echo do_shortcode( "[yith_wcwl_add_to_wishlist]" );
                        echo '</div>';
                    }
                    // Quick View button
                    if ( class_exists( 'YITH_WCQV' ) ) {
                        echo '<div class="component quick-view">';
                            echo '<a href="'.esc_url('#') .'" class="button yith-wcqv-button" data-tooltip="'.esc_attr__('Quickview', 'ibid').'" data-product_id="' . esc_attr(yit_get_prop( $product, 'id', true )) . '"><i class="fa fa-search"></i></a>';
                        echo '</div>';
                    }
                echo '</div>';
            echo '</div>';
        echo '</div>';
    }
    add_action( 'woocommerce_after_shop_loop_item_title', 'ibid_woocommerce_show_price_and_review' );
}



function ibid_woocommerce_get_sidebar() {
    global $ibid_redux;

    if ( is_shop() ) {
        if (is_active_sidebar($ibid_redux['ibid_shop_layout_sidebar'])) {
            dynamic_sidebar( $ibid_redux['ibid_shop_layout_sidebar'] );
        }else{
            if (is_active_sidebar('woocommerce')) {
                dynamic_sidebar( 'woocommerce' );
            } 
        }
    }elseif ( is_product() ) {
        if (is_active_sidebar($ibid_redux['ibid_single_shop_sidebar'])) {
            dynamic_sidebar( $ibid_redux['ibid_single_shop_sidebar'] );
        }else{
            if (is_active_sidebar('woocommerce')) {
                dynamic_sidebar( 'woocommerce' );
            }
        }
    }
}
add_action ( 'woocommerce_sidebar', 'ibid_woocommerce_get_sidebar' );

add_filter( 'loop_shop_columns', 'ibid_wc_loop_shop_columns', 1, 13 );

/*
 * Return a new number of maximum columns for shop archives
 * @param int Original value
 * @return int New number of columns
 */
function ibid_wc_loop_shop_columns( $number_columns ) {
    global $ibid_redux;

    if ( $ibid_redux['ibid-shop-columns'] ) {
        return $ibid_redux['ibid-shop-columns'];
    }else{
        return 3;
    }
}

global $ibid_redux;
if ( isset($ibid_redux['ibid-enable-related-products']) && !$ibid_redux['ibid-enable-related-products'] ) {
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
}

add_filter( 'woocommerce_output_related_products_args', 'ibid_related_products_args' );
  function ibid_related_products_args( $args ) {
    global $ibid_redux;

    $args['posts_per_page'] = $ibid_redux['ibid-related-products-number'];
    $args['columns'] = 4;
    return $args;
}

if ( !function_exists( 'ibid_show_whislist_button_on_single' ) ) {
    function ibid_show_whislist_button_on_single() {
        if ( class_exists( 'YITH_WCWL' ) ) {
            echo '<div class="wishlist-container">';
                echo do_shortcode( "[yith_wcwl_add_to_wishlist]" );
            echo '</div>';
        }
    }
    if ( class_exists( 'YITH_WCWL' ) ) {
        add_action( 'woocommerce_single_product_summary', 'ibid_show_whislist_button_on_single', 36 );
    }
}

//To change wp_register() text:
add_filter('register','ibid_register_text_change');
function ibid_register_text_change($text) {
    $register_text_before   = esc_html__('Site Admin', 'ibid');
    $register_text_after    = esc_html__('Edit Your Profile', 'ibid');

    $text = str_replace($register_text_before, $register_text_after ,$text);

    return $text;
}

//To change wp_loginout() text:
add_filter('loginout','ibid_loginout_text_change');
function ibid_loginout_text_change($text) {
    $login_text_before  = esc_html__('Log in', 'ibid');
    $login_text_after   = esc_html__('Login', 'ibid');

    $logout_text_before = esc_html__('Log', 'ibid');
    $logout_text_after  = esc_html__('Log', 'ibid');

    $text = str_replace($login_text_before, $login_text_after ,$text);
    $text = str_replace($logout_text_before, $logout_text_after ,$text);
    return $text;
}

function ibid_add_editor_styles() {
    add_editor_style( 'css/custom-editor-style.css' );
}
add_action( 'admin_init', 'ibid_add_editor_styles' );


if (!function_exists('ibid_new_loop_shop_per_page')) {
    add_filter( 'loop_shop_per_page', 'ibid_new_loop_shop_per_page', 20 );
    function ibid_new_loop_shop_per_page( $cols ) {
      // $cols contains the current number of products per page based on the value stored on Options -> Reading
      // Return the number of products you wanna show per page.
      $cols = 9;
      return $cols;
    }
}

// KSES ALLOWED HTML
if (!function_exists('ibid_kses_allowed_html')) {
    function ibid_kses_allowed_html($tags, $context) {
      switch($context) {
        case 'link': 
          $tags = array( 
            'a' => array('href' => array()),
          );
          return $tags;
        default: 
          return $tags;
      }
    }
    add_filter( 'wp_kses_allowed_html', 'ibid_kses_allowed_html', 10, 2);
}

if (!function_exists('ibid_redux')) {
    function ibid_redux($redux_meta_name1 = '',$redux_meta_name2 = ''){

        global  $ibid_redux;

        $html = '';
        if (isset($redux_meta_name1) && !empty($redux_meta_name2)) {
            $html = $ibid_redux[$redux_meta_name1][$redux_meta_name2];
        }elseif(isset($redux_meta_name1) && empty($redux_meta_name2)){
            $html = $ibid_redux[$redux_meta_name1];
        }
        
        return $html;
    }
}