<?php
// Logo Source
if (!function_exists('ibid_logo_source')) {
    function ibid_logo_source(){
        
        // REDUX VARIABLE
        global $ibid_redux;
        // html VARIABLE
        $html = '';
        // Metaboxes
        $mt_custom_header_options_status = get_post_meta( get_the_ID(), 'ibid_custom_header_options_status', true );
        $mt_metabox_header_logo = get_post_meta( get_the_ID(), 'ibid_metabox_header_logo', true );
        if (is_page()) {
            if (isset($mt_custom_header_options_status) && isset($mt_metabox_header_logo) && $mt_custom_header_options_status == 'yes') {
                $html .='<img src="'.esc_url($mt_metabox_header_logo).'" alt="'.esc_attr(get_bloginfo()).'" />';
            }else{
                if(!empty($ibid_redux['ibid_logo']['url'])){
                    $html .='<img src="'.esc_url($ibid_redux['ibid_logo']['url']).'" alt="'.esc_attr(get_bloginfo()).'" />';
                }else{ 
                    $html .= $ibid_redux['ibid_logo_text'];
                }
            }
        }else{
            if(!empty($ibid_redux['ibid_logo']['url'])){
                $html .='<img src="'.esc_url($ibid_redux['ibid_logo']['url']).'" alt="'.esc_attr(get_bloginfo()).'" />';
            }elseif(isset($ibid_redux['ibid_logo_text'])){ 
                $html .= $ibid_redux['ibid_logo_text'];
            }else{
                $html .= esc_html(get_bloginfo());
            }
        }
        return $html; 
    }
}
// Logo Area
if (!function_exists('ibid_logo')) {
    function ibid_logo(){
    if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
        global $ibid_redux;
        // html VARIABLE
        $html = '';
        $html .='<h1 class="logo logo-image">';
            $html .='<a href="'.esc_url(get_site_url()).'">';
                $html .= ibid_logo_source();
            $html .='</a>';
        $html .='</h1>';
        return $html;
        // REDUX VARIABLE
     } else {
        global $ibid_redux;
        // html VARIABLE
        $html = '';
        $html .='<h1 class="logo logo-h">';
            $html .='<a href="'.esc_url(get_site_url()).'">';
                $html .= esc_html(get_bloginfo());
            $html .='</a>';
        $html .='</h1>';
        return $html;
     } 
    }
}
// Add specific CSS class by filter
if (!function_exists('ibid_body_classes')) {
    function ibid_body_classes( $classes ) {
        global  $ibid_redux;
        $plugin_redux_status = '';
        if ( ! class_exists( 'ReduxFrameworkPlugin' ) ) {
            $plugin_redux_status = 'missing-redux-framework';
        }
        $plugin_modeltheme_status = '';
        if ( ! class_exists( 'ReduxFrameworkPlugin' ) ) {
            $plugin_modeltheme_status = 'missing-modeltheme-framework';
        }
        // CHECK IF FEATURED IMAGE IS FALSE(Disabled)
        $post_featured_image = '';
        if (is_singular('post')) {
            if ($ibid_redux['post_featured_image'] == false) {
                $post_featured_image = 'hide_post_featured_image';
            }else{
                $post_featured_image = '';
            }
        }
        // CHECK IF THE NAV IS STICKY
        $is_nav_sticky = '';
        if ($ibid_redux['is_nav_sticky'] == true) {
            // If is sticky
            $is_nav_sticky = 'is_nav_sticky';
        }else{
            // If is not sticky
            $is_nav_sticky = '';
        }
        // DIFFERENT HEADER LAYOUT TEMPLATES
        if (is_page()) {
            $mt_custom_header_options_status = get_post_meta( get_the_ID(), 'ibid_custom_header_options_status', true );
            $mt_header_custom_variant = get_post_meta( get_the_ID(), 'ibid_header_custom_variant', true );
            $header_version = 'first_header';
            if (isset($mt_custom_header_options_status) AND $mt_custom_header_options_status == 'yes') {
                if ($mt_header_custom_variant == '1') {
                    // Header Layout #1
                    $header_version = 'first_header';
                }elseif ($mt_header_custom_variant == '2') {
                    // Header Layout #2
                    $header_version = 'second_header';
                }elseif ($mt_header_custom_variant == '3') {
                    // Header Layout #3
                    $header_version = 'third_header';
                }elseif ($mt_header_custom_variant == '4') {
                    // Header Layout #4
                    $header_version = 'fourth_header';
                }elseif ($mt_header_custom_variant == '5') {
                    // Header Layout #5
                    $header_version = 'fifth_header';
                }elseif ($mt_header_custom_variant == '6') {
                    // Header Layout #6
                    $header_version = 'sixth_header';
                }elseif ($mt_header_custom_variant == '7') {
                    // Header Layout #7
                    $header_version = 'seventh_header';
                }elseif ($mt_header_custom_variant == '8') {
                    // Header Layout #8
                    $header_version = 'eighth_header';
                }else{
                    // if no header layout selected show header layout #1
                    $header_version = 'first_header';
                }
            }else{
                if ($ibid_redux['header_layout'] == 'first_header') {
                    // Header Layout #1
                    $header_version = 'first_header';
                }elseif ($ibid_redux['header_layout'] == 'second_header') {
                    // Header Layout #2
                    $header_version = 'second_header';
                }elseif ($ibid_redux['header_layout'] == 'third_header') {
                    // Header Layout #3
                    $header_version = 'third_header';
                }elseif ($ibid_redux['header_layout'] == 'fourth_header') {
                    // Header Layout #4
                    $header_version = 'fourth_header';
                }elseif ($ibid_redux['header_layout'] == 'fifth_header') {
                    // Header Layout #5
                    $header_version = 'fifth_header';
                }elseif ($ibid_redux['header_layout'] == 'sixth_header') {
                    // Header Layout #6
                    $header_version = 'sixth_header';
                }elseif ($ibid_redux['header_layout'] == 'seventh_header') {
                    // Header Layout #7
                    $header_version = 'seventh_header';
                }elseif ($ibid_redux['header_layout'] == 'eighth_header') {
                    // Header Layout #8
                    $header_version = 'eighth_header';
                }else{
                    // if no header layout selected show header layout #1
                    $header_version = 'first_header';
                }
            }
        }else{
            if ($ibid_redux['header_layout'] == 'first_header') {
                // Header Layout #1
                $header_version = 'first_header';
            }elseif ($ibid_redux['header_layout'] == 'second_header') {
                // Header Layout #2
                $header_version = 'second_header';
            }elseif ($ibid_redux['header_layout'] == 'third_header') {
                // Header Layout #3
                $header_version = 'third_header';
            }elseif ($ibid_redux['header_layout'] == 'fourth_header') {
                // Header Layout #4
                $header_version = 'fourth_header';
            }elseif ($ibid_redux['header_layout'] == 'fifth_header') {
                // Header Layout #5
                $header_version = 'fifth_header';
            }elseif ($ibid_redux['header_layout'] == 'sixth_header') {
                // Header Layout #6
                $header_version = 'sixth_header';
            }elseif ($ibid_redux['header_layout'] == 'seventh_header') {
                // Header Layout #7
                $header_version = 'seventh_header';
            }elseif ($ibid_redux['header_layout'] == 'eighth_header') {
                // Header Layout #8
                $header_version = 'eighth_header';
            }else{
                // if no header layout selected show header layout #1
                $header_version = 'first_header';
            }
        }
        // add 'class-name' to the $classes array
        $classes[] = esc_attr($plugin_modeltheme_status) . ' ' . esc_attr($plugin_redux_status) . ' ' . esc_attr($is_nav_sticky) . ' ' . esc_attr($header_version) . ' ' . esc_attr($post_featured_image) . ' ';
        // return the $classes array
        return $classes;
    }
    add_filter( 'body_class', 'ibid_body_classes' );
}